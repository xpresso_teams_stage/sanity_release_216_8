""" Parses configuration file to create an internal dictionary file"""

__all__ = ['XprConfigParser']
__author__ = 'Naveen Sinha'

import base64
import json
import os
from functools import reduce
from json import JSONDecodeError
from operator import getitem

import jsonmerge
from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Hash import SHA256

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidConfigException, PasswordEncryptionError
from xpresso.ai.core.commons.utils.constants import FILE_ENCRYPTION_KEY, \
    VAULT_CONFIG, VAULT_IP_CONFIG, VAULT_PORT_CONFIG, VAULT_TOKEN_HEADER, \
    VAULT_TOKEN_CONFIG, USE_VAULT_PASSWORDS_ENV, CACHE_LENGTH_LIMIT, \
    CACHE_SIZE_LIMIT, EMPTY_STRING
from xpresso.ai.core.commons.utils.generic_utils import get_base_pkg_location, \
    get_default_config_path, str2bool
from xpresso.ai.core.commons.utils.singleton import Singleton
from xpresso.ai.core.commons.utils.xpr_vault import XprVault


class XprConfigParser(metaclass=Singleton):
    """ Parses properties file and stores them in the internal dictionary file.
    It expects the location of config file as an input. By Default, it searches
    config in config/common.json.
    """

    # This is used when no configuration is provided
    DEFAULT_CONFIG_PATH = get_default_config_path()
    BASE_CONFIG_PATH = "config/common.json"
    PASSWORD_KEYS_VAULT = {
        'vms': ('vms', 'password'),
        'vms_guest': ('vms', 'guest_login', 'password'),
        'jenkins': ('build_management', 'jenkins', 'password'),
        'bitbucket': ('code_management', 'bitbucket', 'password'),
        'email_notif': ('email_notification', 'sender_passwd'),
        'docker_registry': ('docker_registry', 'password'),
        'mongodb': ('mongodb', 'mongo_pwd'),
        "email_password": ("email_notification", "sender_passwd")
    }
    PASSWORD_KEYS = [(VAULT_CONFIG, VAULT_TOKEN_CONFIG)]
    ENCRYPTION_KEY = FILE_ENCRYPTION_KEY
    config_file_path = ""

    def __init__(self, config_file_path=DEFAULT_CONFIG_PATH,
                 use_base=True, encrypt=False, save_file=False):

        self.config_json = None
        if use_base:
            self.config_file_path = self.get_default_config_path()
            self.populate_config(config_path=self.config_file_path)
        if config_file_path != self.BASE_CONFIG_PATH:
            self.config_file_path = os.path.join(get_base_pkg_location(),
                                                 config_file_path)
            self.populate_config(config_path=self.config_file_path)

        # encrypt tells whether the file needs to be encrypted or decrypted
        # Default is False (decryption)
        self.file_encryption(encrypt=encrypt, save_file=save_file)
        if str2bool(os.environ.get(USE_VAULT_PASSWORDS_ENV, "True")):
            self.fill_passwords_from_vault(encrypt=encrypt)
        print(222222222, self.config_json)

    def get_default_config_path(self):
        """ Returns the absolute path of default configuration file """
        return os.path.join(get_base_pkg_location(), self.BASE_CONFIG_PATH)

    def populate_config(self, config_path: str):
        """ Loads configuration and merge it with the previous configuration.
            Also fills in passwords from the vault.
         Args:
             config_path(str): path to the configuration file
        """
        if not self.config_json:
            self.config_json = {}
        self.config_json = jsonmerge.merge(
            self.config_json,
            self.parse_json(config_path=config_path))

    @staticmethod
    def parse_json(config_path: str):
        """ Parses the JSON configuration
        Args:
             config_path(str): path to the configuration file

        Returns:
            Json Object: parsed json object
        Throws:
            InvalidConfigException: raised if there is an issue with the json
                                    file
        """
        try:
            json_fs = open(config_path, 'r', encoding='utf-8')
            json_object = json.load(json_fs)
            return json_object
        except FileNotFoundError as e:
            raise InvalidConfigException("{} config file path is invalid. {}"
                                         .format(config_path, str(e)))
        except (JSONDecodeError, TypeError) as e:
            raise InvalidConfigException("{} is invalid json in config. {}"
                                         .format(config_path, str(e)))

    def __getitem__(self, key):
        """
        Overriding get method to support direct return from self.config_json
        """
        return self.config_json[key]

    def to_json(self, json_file_path):
        """
        Stores current config into a json object
        Args:
            json_file_path: output file path where json is stored
        """
        try:
            with open(json_file_path, "w") as json_fp:
                json.dump(self.config_json, json_fp, indent=4)
            return True
        except(PermissionError, FileNotFoundError, FileExistsError) as e:
            print(e)
        return False

    def file_encryption(self, encrypt=False, save_file=False):
        """
        Saves the encrypted passwords in the json and decrypts the password when loading config
        Args:
            encrypt:
                - True:  Encrypt the passwords
                - False: Decrypt the passwords
            save_file:
                -True: Saves the config back to the location after encryption
                -False: Does not store the config
        """
        if encrypt:
            for nested_keys in self.PASSWORD_KEYS:
                try:
                    password = reduce(dict.get, nested_keys, self.config_json)
                except (KeyError, TypeError):
                    continue
                try:
                    decrypted = self.decrypt(self.ENCRYPTION_KEY, password)
                    encrypted = self.encrypt(self.ENCRYPTION_KEY, decrypted)
                except PasswordEncryptionError:
                    encrypted = self.encrypt(self.ENCRYPTION_KEY, password)

                reduce(getitem, nested_keys[:-1], self.config_json)[nested_keys[-1]] = encrypted

            if save_file:
                self.to_json(self.config_file_path)
        else:
            for nested_keys in self.PASSWORD_KEYS:
                try:
                    encrypted_password = reduce(dict.get, nested_keys, self.config_json)
                except (TypeError, KeyError) as err:
                    continue
                decrypted = self.decrypt(self.ENCRYPTION_KEY, encrypted_password)
                reduce(getitem, nested_keys[:-1], self.config_json)[nested_keys[-1]] = decrypted
            if save_file:
                self.to_json(self.config_file_path)

    @staticmethod
    def encrypt(key, password, encode=True):
        """
        Encrypt the given password using the SHA256 and AES
        Args:
            key: Key used to encrypt password
            password: The password that needs to be encrypted
            encode: True b64-encoding is performed on the encrypted password
        """
        try:
            key = SHA256.new(key).digest()
            IV = Random.new().read(AES.block_size)
            encryptor = AES.new(key, AES.MODE_CBC, IV)
            padding = AES.block_size - len(
                password) % AES.block_size  # Calculates the needed padding

            password += (bytes([padding]) * padding).decode("utf-8")
            data = IV + encryptor.encrypt(password.encode('utf-8'))
            return base64.b64encode(data).decode("utf-8") if encode else data

        except (ValueError, AttributeError) as err:
            raise PasswordEncryptionError(f"Failed to encrypt passwords: {str(err)}")

    @staticmethod
    def decrypt(key, password, decode=True):
        """
        Decodes the given password using the SHA256 and AES
        Args:
            key: Key used to encrypt password
            password: The password that needs to be encrypted
            decode: True b64-decoding is performed on the decrypted password
        """
        try:
            if decode:
                password = base64.b64decode(password.encode("utf-8"))
            key = SHA256.new(key).digest()
            IV = password[:AES.block_size]
            decryptor = AES.new(key, AES.MODE_CBC, IV)

            # Decrypt
            data = decryptor.decrypt(password[AES.block_size:])
            padding = data[-1]
            if data[-padding:] != bytes([padding]) * padding:
                raise PasswordEncryptionError("Failed to Decrypt the password")
            return data[:-padding].decode("utf-8")
        except ValueError:
            raise PasswordEncryptionError

    def fill_passwords_from_vault(self, encrypt):
        """
        Fetches passwords from vault and fills them at the appropriate
        location in config
        Args:
            encrypt(bool): True if encryption was done, else False
        """
        token = self.config_json[VAULT_CONFIG][VAULT_TOKEN_CONFIG]
        if encrypt:
            headers = {VAULT_TOKEN_HEADER: self.decrypt(self.ENCRYPTION_KEY,
                                                        token)}
        else:
            headers = {VAULT_TOKEN_HEADER: token}
        ip = self.config_json[VAULT_CONFIG][VAULT_IP_CONFIG]
        port = self.config_json[VAULT_CONFIG][VAULT_PORT_CONFIG]
        max_length = self.config_json[VAULT_CONFIG][CACHE_LENGTH_LIMIT]
        max_size = self.config_json[VAULT_CONFIG][CACHE_SIZE_LIMIT]
        vault = XprVault(vault_ip=ip, vault_port=port, request_headers=headers,
                         max_length=max_length, max_size=max_size)
        pwd = vault.fetch_passwords()
        for vault_key, pwd_keys in self.PASSWORD_KEYS_VAULT.items():
            if vault_key not in pwd:
                raise InvalidConfigException(
                    f'Failed to fetch password for the key "{vault_key}". '
                    f'Please contact the xpresso team.')
            reduce(getitem, pwd_keys[:-1], self.config_json)[
                pwd_keys[-1]] = pwd[vault_key]

    def remove_passwords_from_config(self):
        for vault_key, pwd_keys in self.PASSWORD_KEYS_VAULT.items():
            reduce(getitem, pwd_keys[:-1], self.config_json)[
                pwd_keys[-1]] = EMPTY_STRING

